# coding: utf-8

import os
from os.path import exists
import requests
from urllib.request import Request, urlopen

voice_url = "https://everyayah.com/data/{reciter}/{aya}.mp3"

reciters = [
	{"src":"AbdulSamad_64kbps_QuranExplorer.Com", "dest":"abdulsamad"},
	#{"src":"Abdul_Basit_Mujawwad_128kbps", "dest":"abdulsamad_mujawwad"},
	#{"src":"Abdurrahmaan_As-Sudais_192kbps", "dest":"abdurrahmaan_as-sudais"}
]

suras = [
	{"id":1, "nbaya":7},
	{"id":2, "nbaya":286},
	{"id":3, "nbaya":200},
	{"id":4, "nbaya":176},
	{"id":5, "nbaya":120},
	{"id":6, "nbaya":165},
	{"id":7, "nbaya":206},
	{"id":8, "nbaya":75},
	{"id":9, "nbaya":129},
]

for reciter in reciters:
	#print(reciter['src'])
	
	if not os.path.exists(reciter['dest']):
		os.makedirs(reciter['dest'])
   
	for sura in suras:
		for idaya in range(1,sura['nbaya'] + 1):
		
			id = str(sura['id']).zfill(3) + str(idaya).zfill(3)
			recitation_path = reciter['dest'] + '/' + reciter['dest']+'_' + id + '.mp3'
			
			if(not exists(recitation_path)):
				url = voice_url.replace('{reciter}', reciter['src']).replace('{aya}', id)
		
				print(url)
		
				doc = requests.get(url)
		
				with open(recitation_path, 'wb') as f:
					f.write(doc.content)
				
			
