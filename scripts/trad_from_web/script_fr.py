# coding: utf-8

# import csv
import json
import os
from os.path import basename
from zipfile import ZipFile
from urllib.request import Request, urlopen

sura_names = [
	['الفاتحة', "Al-Faatiha", 'The Opening', 'Meccan'],
	['البقرة', "Al-Baqara", 'The Cow', 'Medinan'],
	['آل عمران', "Aal-i-Imraan", 'The Family of Imraan', 'Medinan'],
	['النساء', "An-Nisaa", 'The Women', 'Medinan'],
	['المائدة', "Al-Maaida", 'The Table', 'Medinan'],
	['الأنعام', "Al-An'aam", 'The Cattle', 'Meccan'],
	['الأعراف', "Al-A'raaf", 'The Heights', 'Meccan'],
	['الأنفال', "Al-Anfaal", 'The Spoils of War', 'Medinan'],
	['التوبة', "At-Tawba", 'The Repentance', 'Medinan'],
	['يونس', "Yunus", 'Jonas', 'Meccan'],
	['هود', "Hud", 'Hud', 'Meccan'],
	['يوسف', "Yusuf", 'Joseph', 'Meccan'],
	['الرعد', "Ar-Ra'd", 'The Thunder', 'Medinan'],
	['ابراهيم', "Ibrahim", 'Abraham', 'Meccan'],
	['الحجر', "Al-Hijr", 'The Rock', 'Meccan'],
	['النحل', "An-Nahl", 'The Bee', 'Meccan'],
	['الإسراء', "Al-Israa", 'The Night Journey', 'Meccan'],
	['الكهف', "Al-Kahf", 'The Cave', 'Meccan'],
	['مريم', "Maryam", 'Mary', 'Meccan'],
	['طه', "Taa-Haa", 'Taa-Haa', 'Meccan'],
	['الأنبياء', "Al-Anbiyaa", 'The Prophets', 'Meccan'],
	['الحج', "Al-Hajj", 'The Pilgrimage', 'Medinan'],
	['المؤمنون', "Al-Muminoon", 'The Believers', 'Meccan'],
	['النور', "An-Noor", 'The Light', 'Medinan'],
	['الفرقان', "Al-Furqaan", 'The Criterion', 'Meccan'],
	['الشعراء', "Ash-Shu'araa", 'The Poets', 'Meccan'],
	['النمل', "An-Naml", 'The Ant', 'Meccan'],
	['القصص', "Al-Qasas", 'The Stories', 'Meccan'],
	['العنكبوت', "Al-Ankaboot", 'The Spider', 'Meccan'],
	['الروم', "Ar-Room", 'The Romans', 'Meccan'],
	['لقمان', "Luqman", 'Luqman', 'Meccan'],
	['السجدة', "As-Sajda", 'The Prostration', 'Meccan'],
	['الأحزاب', "Al-Ahzaab", 'The Clans', 'Medinan'],
	['سبإ', "Saba", 'Sheba', 'Meccan'],
	['فاطر', "Faatir", 'The Originator', 'Meccan'],
	['يس', "Yaseen", 'Yaseen', 'Meccan'],
	['الصافات', "As-Saaffaat", 'Those drawn up in Ranks', 'Meccan'],
	['ص', "Saad", 'The letter Saad', 'Meccan'],
	['الزمر', "Az-Zumar", 'The Groups', 'Meccan'],
	['غافر', "Al-Ghaafir", 'The Forgiver', 'Meccan'],
	['فصلت', "Fussilat", 'Explained in detail', 'Meccan'],
	['الشورى', "Ash-Shura", 'Consultation', 'Meccan'],
	['الزخرف', "Az-Zukhruf", 'Ornaments of gold', 'Meccan'],
	['الدخان', "Ad-Dukhaan", 'The Smoke', 'Meccan'],
	['الجاثية', "Al-Jaathiya", 'Crouching', 'Meccan'],
	['الأحقاف', "Al-Ahqaf", 'The Dunes', 'Meccan'],
	['محمد', "Muhammad", 'Muhammad', 'Medinan'],
	['الفتح', "Al-Fath", 'The Victory', 'Medinan'],
	['الحجرات', "Al-Hujuraat", 'The Inner Apartments', 'Medinan'],
	['ق', "Qaaf", 'The letter Qaaf', 'Meccan'],
	['الذاريات', "Adh-Dhaariyat", 'The Winnowing Winds', 'Meccan'],
	['الطور', "At-Tur", 'The Mount', 'Meccan'],
	['النجم', "An-Najm", 'The Star', 'Meccan'],
	['القمر', "Al-Qamar", 'The Moon', 'Meccan'],
	['الرحمن', "Ar-Rahmaan", 'The Beneficent', 'Medinan'],
	['الواقعة', "Al-Waaqia", 'The Inevitable', 'Meccan'],
	['الحديد', "Al-Hadid", 'The Iron', 'Medinan'],
	['المجادلة', "Al-Mujaadila", 'The Pleading Woman', 'Medinan'],
	['الحشر', "Al-Hashr", 'The Exile', 'Medinan'],
	['الممتحنة', "Al-Mumtahana", 'She that is to be examined', 'Medinan'],
	['الصف', "As-Saff", 'The Ranks', 'Medinan'],
	['الجمعة', "Al-Jumu'a", 'Friday', 'Medinan'],
	['المنافقون', "Al-Munaafiqoon", 'The Hypocrites', 'Medinan'],
	['التغابن', "At-Taghaabun", 'Mutual Disillusion', 'Medinan'],
	['الطلاق', "At-Talaaq", 'Divorce', 'Medinan'],
	['التحريم', "At-Tahrim", 'The Prohibition', 'Medinan'],
	['الملك', "Al-Mulk", 'The Sovereignty', 'Meccan'],
	['القلم', "Al-Qalam", 'The Pen', 'Meccan'],
	['الحاقة', "Al-Haaqqa", 'The Reality', 'Meccan'],
	['المعارج', "Al-Ma'aarij", 'The Ascending Stairways', 'Meccan'],
	['نوح', "Nooh", 'Noah', 'Meccan'],
	['الجن', "Al-Jinn", 'The Jinn', 'Meccan'],
	['المزمل', "Al-Muzzammil", 'The Enshrouded One', 'Meccan'],
	['المدثر', "Al-Muddaththir", 'The Cloaked One', 'Meccan'],
	['القيامة', "Al-Qiyaama", 'The Resurrection', 'Meccan'],
	['الانسان', "Al-Insaan", 'Man', 'Medinan'],
	['المرسلات', "Al-Mursalaat", 'The Emissaries', 'Meccan'],
	['النبإ', "An-Naba", 'The Announcement', 'Meccan'],
	['النازعات', "An-Naazi'aat", 'Those who drag forth', 'Meccan'],
	['عبس', "Abasa", 'He frowned', 'Meccan'],
	['التكوير', "At-Takwir", 'The Overthrowing', 'Meccan'],
	['الإنفطار', "Al-Infitaar", 'The Cleaving', 'Meccan'],
	['المطففين', "Al-Mutaffifin", 'Defrauding', 'Meccan'],
	['الإنشقاق', "Al-Inshiqaaq", 'The Splitting Open', 'Meccan'],
	['البروج', "Al-Burooj", 'The Constellations', 'Meccan'],
	['الطارق', "At-Taariq", 'The Morning Star', 'Meccan'],
	['الأعلى', "Al-A'laa", 'The Most High', 'Meccan'],
	['الغاشية', "Al-Ghaashiya", 'The Overwhelming', 'Meccan'],
	['الفجر', "Al-Fajr", 'The Dawn', 'Meccan'],
	['البلد', "Al-Balad", 'The City', 'Meccan'],
	['الشمس', "Ash-Shams", 'The Sun', 'Meccan'],
	['الليل', "Al-Lail", 'The Night', 'Meccan'],
	['الضحى', "Ad-Dhuhaa", 'The Morning Hours', 'Meccan'],
	['الشرح', "Ash-Sharh", 'The Consolation', 'Meccan'],
	['التين', "At-Tin", 'The Fig', 'Meccan'],
	['العلق', "Al-Alaq", 'The Clot', 'Meccan'],
	['القدر', "Al-Qadr", 'The Power, Fate', 'Meccan'],
	['البينة', "Al-Bayyina", 'The Evidence', 'Medinan'],
	['الزلزلة', "Az-Zalzala", 'The Earthquake', 'Medinan'],
	['العاديات', "Al-Aadiyaat", 'The Chargers', 'Meccan'],
	['القارعة', "Al-Qaari'a", 'The Calamity', 'Meccan'],
	['التكاثر', "At-Takaathur", 'Competition', 'Meccan'],
	['العصر', "Al-Asr", 'The Declining Day, Epoch', 'Meccan'],
	['الهمزة', "Al-Humaza", 'The Traducer', 'Meccan'],
	['الفيل', "Al-Fil", 'The Elephant', 'Meccan'],
	['قريش', "Quraish", 'Quraysh', 'Meccan'],
	['الماعون', "Al-Maa'un", 'Almsgiving', 'Meccan'],
	['الكوثر', "Al-Kawthar", 'Abundance', 'Meccan'],
	['الكافرون', "Al-Kaafiroon", 'The Disbelievers', 'Meccan'],
	['النصر', "An-Nasr", 'Divine Support', 'Medinan'],
	['المسد', "Al-Masad", 'The Palm Fibre', 'Meccan'],
	['الإخلاص', "Al-Ikhlaas", 'Sincerity', 'Meccan'],
	['الفلق', "Al-Falaq", 'The Dawn', 'Meccan'],
	['الناس', "An-Naas", 'Mankind', 'Meccan']
]


translations_file_path = 'out/trad_list_xxx.csv'

list_translations_url = "https://quranenc.com/api/v1/translations/list"
sura_translation_url = "https://quranenc.com/api/v1/translation/sura/{translation_key}/{sura_number}"
# Tafsir : https://quranenc.com/fr/ajax/tafsir/katheer/1/7

req = Request(list_translations_url)
req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36')
data = urlopen(req).read()
jsondata = json.loads(data)

#jsondata = {'translations': [{'key': 'french_hameedullah', 'language_iso_code': 'fr', 'version': '1.0.5', 'last_update': 1689494567, 'title': 'French Translation - Muhammad Hamidullah', 'description': ''}]}

with open(translations_file_path, 'w') as td:

	for translation in jsondata['translations']:
		print('Process ' + translation['key'])
		trad_key = translation['key']
		trad_name = translation['title']
		trad_description = translation['description']
		trad_language_iso_code = translation['language_iso_code']

		output_file_path = 'out/trad_'+trad_key+'.csv'

		with open(output_file_path, 'w') as d:

			td.write("trad_"+trad_key+"|"+trad_name+"|"+trad_language_iso_code+"|TEXT|https://gitlab.com/redabourebaba/quran_data/-/raw/main/traductions/trad_"+trad_key+".csv.zip"+"\n")

			for i in range(114):
			#for i in range(2):

				url = sura_translation_url.replace("{sura_number}", str(i+1)).replace("{translation_key}", trad_key)

				d.write(str(i+1)+"|"+sura_names[i][1]+"|\n")

				print(url)

				req2 = Request(url)
				req2.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36')
				traddata = urlopen(req2).read()
				tradjsondata = json.loads(traddata)

				for aya in tradjsondata['result']:
					d.write(aya['sura'] + aya['aya'].zfill(3) + "|" + aya['translation'] + "|" + (aya['footnotes'].replace("\n", "<br>") if aya['footnotes'] is not None else "") + "\n")
		
		with ZipFile(output_file_path+'.zip', 'w') as fout:
			fout.write(output_file_path, basename(output_file_path))
		
		os.remove(output_file_path)
