# coding: utf-8

import json

lang = 'fr'
trad_uid = 'trad_fr'
trad_name = 'Traduction Française'
src_file_path = 'in/quran_fr.json'
output_file_path = 'out/trad_fr.sql'

with open(src_file_path, encoding='utf-8') as f:

    data = json.load(f)

    with open(output_file_path, 'w') as d:

        d.write("insert into trad (uid, name, lang) values ('"+trad_uid+"', '"+trad_name+"', '"+lang+"')\n")

        for sura in data:
            suraId = sura['id']

            suraName = (sura['transliteration']+"("+sura['translation']+")").replace('\'','\'\'')

            # Insert Sura traduction
            d.write("insert into trad_detail (object_id,trad_uid,value) values ("+str(suraId)+",'"+trad_uid+"', '"+suraName+"')\n")

            st = "insert into trad_detail (object_id,trad_uid,value) values "
            for verse in sura['verses']:
                verseId = verse['id']
                versetext = verse['translation'].replace('\'','\'\'')
                st = st + "("+str(suraId*1000+verseId)+",'"+trad_uid+"','"+versetext+"'),"
            st = st[:-1]+";\n"
            d.write(st)
