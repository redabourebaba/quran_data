# coding: utf-8

import json
import gzip
import shutil

lang = 'ar'
trad_uid = 'trad_tafseer_moyaser'
trad_name = 'Tafseer Moyaser'
src_file_path = 'in/tafseer.json'
output_file_path = 'out/trad_tafseer_moyaser.csv'

with open(src_file_path, encoding='utf-8') as f:
    data = json.load(f)
    with open(output_file_path, 'w') as d:
        for verse in data:
            suraId = int(verse['number'])
            verseId = int(verse['aya'])
            verseText = verse['text'].replace('\'','\'\'')
            d.write(str(suraId*1000+verseId)+"|"+verseText+"\n")

with open(output_file_path, 'rb') as f_in:
    with gzip.open(output_file_path+'.gz', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)
