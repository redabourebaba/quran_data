# coding: utf-8

import json
import gzip
import shutil

lang = 'fr'
trad_uid = 'trad_fr'
trad_name = 'Traduction Française'
src_file_path = 'in/quran_fr.json'
output_file_path = 'out/trad_fr.csv'

with open(src_file_path, encoding='utf-8') as f:
    data = json.load(f)
    with open(output_file_path, 'w') as d:
        for sura in data:
            suraId = sura['id']
            suraName = (sura['transliteration']+"("+sura['translation']+")")
            # Insert Sura traduction
            d.write(str(suraId)+"|"+suraName+"\n")
            for verse in sura['verses']:
                verseId = verse['id']
                verseText = verse['translation']
                d.write(str(suraId*1000+verseId)+"|"+verseText+"\n")

with open(output_file_path, 'rb') as f_in:
    with gzip.open(output_file_path+'.gz', 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)