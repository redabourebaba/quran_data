# coding: utf-8

import json

f = open('in/quran.json', 'r')

data = json.load(f)

with open('out/trad_fr.sql', 'w') as d:

    d.write("insert into trad (uid, name, lang) values ('aya_fr', 'Traduction Française', 'fr')\n")

    for sourat in data['sourates']:
        souratId = sourat['position']
        st = "insert into trad_detail (object_id,trad_uid,value) values "
        for verset in sourat['versets']:
            versetId = verset['position_ds_sourate']
            st = st + "("+str(souratId*1000+versetId)+",'aya_fr','"+verset['text'].encode('utf-8').replace('\'','\'\'')+"'),"
        st = st[:-1]+";\n"
        d.write(st)

