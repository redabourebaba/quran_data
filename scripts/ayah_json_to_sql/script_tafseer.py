# coding: utf-8

import json

lang = 'ar'
trad_uid = 'trad_tafseer_moyaser_ar'
trad_name = 'Tafseer Moyaser'
src_file_path = 'in/tafseer.json'
output_file_path = 'out/tafseer_moyaser_ar.sql'

with open(src_file_path, encoding='utf-8') as f:

    data = json.load(f)

    with open(output_file_path, 'w') as d:

        d.write("insert into trad (uid, name, lang) values ('"+trad_uid+"', '"+trad_name+"', '"+lang+"')\n")

        st = "insert into trad_detail (object_id,trad_uid,value) values "
        oldSouratId = 1
        for verse in data:
            suraId = int(verse['number'])
            verseId = int(verse['aya'])

            if oldSouratId != suraId:
                oldSouratId = suraId
                st = st[:-1]+";\n"
                d.write(st)
                st = "insert into trad_detail (object_id,trad_uid,value) values "
            versetext = verse['text'].replace('\'','\'\'')
            st = st + "("+str(suraId*1000+verseId)+",'"+trad_uid+"','"+versetext+"'),"
        st = st[:-1]+";\n"
        d.write(st)